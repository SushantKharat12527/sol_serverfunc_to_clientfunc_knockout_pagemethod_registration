﻿using Newtonsoft.Json;
using Registration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Registration
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #region Web Api
        [WebMethod]
        public static int? CheckLoginApi(String userEntityJson)
        {
            int? status = 0;
            string message = null;
            RegisterDCDataContext _db = new RegisterDCDataContext();

            try
            {

                if (userEntityJson != null)
                {


                    UserEntity userEntityObj = JsonConvert.DeserializeObject<UserEntity>(userEntityJson);

                    _db
                        ?.uspSetUser(
                            "Insert"
                            ,null
                            ,userEntityObj.FirstName
                            ,userEntityObj.LastName
                            ,userEntityObj.PersonType
                            ,userEntityObj.Login.UserName
                            , userEntityObj.Login.Password
                            ,userEntityObj.Communication.MobileNo
                            ,userEntityObj.Communication.EmailId
                            ,ref status
                            ,ref message
                            );


                    return status;
                }


            }
            catch (Exception)
            {
                throw;
            }

            return status;
        }
        #endregion 
    }
}