﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Registration.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

   <%--  <style>
        body{
            padding : 10px;
            margin:auto;
        }

        .divCenterPosition{
            margin:0px auto;
            margin-top:150px;
        }

        .errorPanelHide{
            display:none;
        }
    </style>--%>

     
    <script type="text/javascript" src="Scripts/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="Scripts/knockout-3.4.2.js"></script>

 <script type="text/javascript">
        function pageLoad() {

            $(document).ready(function () {

              

                loginViewModel();

               

            });

            function loginViewModel()
            {
                var model = {
                    firstName: ko.observable(),
                    lastName: ko.observable(),
                    personType:ko.observable(),
                    userName: ko.observable(), 
                    password: ko.observable(),
                    mobileNo: ko.observable(),
                    emailId:ko.observable(),


                    loginClick: function () {
                    
                        var loginJson = ko.toJSON({
                            FirstName: model.firstName(),
                            LastName:model.lastName(),
                            PersonType: model.personType(),
                            Login:{    
                                UserName: model.userName(),
                                Password: model.password()
                            },
                            Communication: {
                                MobileNo: model.mobileNo(),
                                EmailId:model.emailId()
                            }
                        
                            
                        });

                        alert(loginJson);

                        
                        PageMethods.CheckLoginApi(loginJson, function (result) {

                            alert("Api Call");

                            alert(result);

                            if (result == 1)
                            {
                                
                                $("#lblMessage").text("Registered Succesfully");
                              
                                $("#divMessagePanel")
                                    .show()
                                    .fadeOut(3000, function () {

                                   

                                });
                            }
                            else
                            {
                                  
                                $("#lblMessage").text("Registration Failed");
                                
                                $("#divMessagePanel")
                                    .show()
                                    .fadeOut(3000, function () {

                                        $("#txtFirstName").val("");
                                        $("#txtLastName").val("");
                                        $("#txtPersonType").val("");
                                        $("#txtUserName").val("");
                                        $("#txtPassword").val("");
                                        $("#txtMobileNo").val("");
                                        $("#txtEmailId").val("");

                                        $("#txtFirstName").focus();

                                });
                            }

                        });


                       
                    }
                }

              

                ko.applyBindings(model);
            }

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        
             <asp:ScriptManager ID="scriptManager" runat="server" EnablePageMethods="true"></asp:ScriptManager>

        <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>

                    <div class="w3-card-4 divCenterPosition" style="width: 40%">
                        <div class="w3-container w3-teal">
                            <h2>Registration</h2>
                        </div>

                        <div class="w3-container w3-white">

                            <table class="w3-table">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtFirstName" placeholder="FirstName" runat="server" CssClass="w3-input w3-border w3-light-grey" data-bind="value: firstName"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtLastName" placeholder="LastName" runat="server" CssClass="w3-input w3-border w3-light-grey" data-bind="value: lastName"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtPersonType" placeholder="PersonType" runat="server" CssClass="w3-input w3-border w3-light-grey" data-bind="value: personType"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtUserName" placeholder="UserName" runat="server" CssClass="w3-input w3-border w3-light-grey" data-bind="value: userName"></asp:TextBox>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtPassword" placeholder="Password" runat="server" TextMode="Password" CssClass="w3-input w3-border w3-light-grey" data-bind="value: password"></asp:TextBox>
                                    </td>
                                </tr>
                               <tr>
                                    <td>
                                        <asp:TextBox ID="txtMobileNo" placeholder="Mobile No" runat="server" CssClass="w3-input w3-border w3-light-grey" data-bind="value: mobileNo"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtEmailId" placeholder="EmailId" runat="server" CssClass="w3-input w3-border w3-light-grey" data-bind="value: emailId"></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <asp:Button ID="btnSubmit" runat="server" AccessKey="N" Text="Sign In" CssClass="w3-btn w3-blue-grey" data-bind="click: loginClick"></asp:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="divMessagePanel" class="w3-panel w3-blue w3-card-4 errorPanelHide">
                                            <span id="lblMessage"></span>
                                        </div>
                                    </td>
                                </tr>
                            </table>

                        </div>

                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>

    </div>
    </form>
</body>
</html>
