﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registration.Models
{
    public class UserEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonType { get; set; }

        public LoginEntity Login { get; set; }
        public CommunicationEntity Communication { get; set; }
    }
}