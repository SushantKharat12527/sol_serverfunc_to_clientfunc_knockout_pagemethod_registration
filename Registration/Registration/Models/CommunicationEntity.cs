﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registration.Models
{
    public class CommunicationEntity
    {
        public string MobileNo { get; set; }
        public string EmailId { get; set; }
    }
}